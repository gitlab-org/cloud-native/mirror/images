#!/bin/sh

if [ $# -eq 0 ] ; then
  echo 'No file specified'
  exit 0
fi

set -e

image_file=$1

if [ ! -f ${image_file} ] ; then
  echo "Image file not found: ${image_file}";
  exit 1
fi

MIRROR_DIR=${MIRROR_DIR:-"mirror"}
DIGEST_DIR=${DIGEST_DIR:-"digest"}
mkdir -p $MIRROR_DIR $DIGEST_DIR

while read -r image mirror_image; do
  mirror_path="docker://${CI_REGISTRY_IMAGE}/${mirror_image}"
  digest_path=${DIGEST_DIR}/$(basename $image)

  if [ -z ${DRY_RUN+x} ]; then
    echo "Pushing ${mirror_path} to mirror registry"
  else
    echo "DRY RUN: not pushing ${mirror_path} to mirror registry."
    echo "Pulling ${image} from public registry"
    mkdir -p "${MIRROR_DIR}/${mirror_image}"
    mirror_path="dir:${MIRROR_DIR}/${mirror_image}"
  fi

  skopeo --insecure-policy copy --all --src-no-creds \
    --digestfile $digest_path \
    docker://${image} ${mirror_path}

  echo "Synced ${digest_path}, manifest digest: $(cat ${digest_path})"
done < "${image_file}"
