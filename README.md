# Images

Mirror of container images consumed by Cloud Native GitLab.

These are images consumed by our [Helm Charts](https://gitlab.com/gitlab-org/charts/gitlab) or [Operator](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator).

This mirror is _not an active mirror_ from Docker Hub. We only mirror the specific
tagged images consumed by related projects.
